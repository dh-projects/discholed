## Discholed

This repository gathers every modification and script I have done during my internship. In this readme, I will describe and explain all the directories in this repository.

### .github/workflows

These are all the files for the GitHub action. It's meant to be used in a GitHub repository with the same directory tree:

```bash
├───.github
│   └───workflows
│           error_comment.md
│           pull_request_checker.yml
│           xml_check.py
```

You will also need to follow this tutorial to prevent push requests if there are any problems: [how-to-auto-reject-a-pull-request-if-tests-are-failing-github-actions](https://stackoverflow.com/questions/58654530/how-to-auto-reject-a-pull-request-if-tests-are-failing-github-actions). The actions will activate on every pull request and then check every .xml file with this [Python script](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/.github/workflows/xml_check.py). If the script returns any errors, it will prevent the pull request and create an issue with an error message explaining where the script found errors. If the pull request is stopped, you will have to correct the errors and then close and reopen the pull request: it will reactivate the GitHub action.

### Discholed

The Discholed directory gathers all the files I worked on from the online application.

```bash
├───Discholed
│   │
│   ├───data
│   │   │
│   │   ├───bi
│   │   │
│   │   ├───cds
│   │   │
│   │   ├───ehri
│   │   │
│   │   ├───maguerre
│   │   │
│   │   ├───nachlassprojekt
│   │   │
│   │   ├───pec
│   │   │
│   │   └───rochlitz
|   |   
│   ├───modules
│   │
│   ├───resources  
│   │
│   ├───templates
│   │   │
│   │   ├───about
│   │   │
│   │   ├───history
│   │   │
│   │   └───pages
│   │
│   └───transform
```

### Livrable/Screen

This folder contains all the screenshots I took of the application before and after every modification I made.

### Pb-component-bundle

In this directory, you can find the [pb-timeline.js](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/Pb-component-bundle/pb-timeline.js). This JavaScript file will create the custom tag `<pb-timeline>`.

### Python Scripts

In this folder, I gathered every Python script I made during my internship.

```bash
└───script PYTHON
    ├───Issue closer
    │       closer.py
    │
    ├───Paragraph
    │       file_modified.csv
    │       file_to_modify.csv
    │       Index_correction.py
    │
    ├───Scrapper
    │       scrapper.py
    │
    ├───Tag_finder
    │       tag_finder.py
    │
    ├───Template_usage
    │       template_finder.py
    │
    └───Timeline
            timeline_bi.xml
            timeline_cds.xml
            timeline_ehri.xml
            timeline_generator.py
            timeline_nachlassprojekt.xml
            timeline_pec.xml
```

- [Issue Closer](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/Python%20Scripts/Issue%20Closer/closer.py): This was created during the development of the Git Actions. The repository where I was testing my script had more than 100 issues, so I created this script to close all of them at once.

- [Paragraph](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/Python%20Scripts/Paragraph/Index_correction.py): This script checks every XML file to determine which one lacks a `<p>` tag inside of a `<pb>` tag (the [issue](https://gitlab.inria.fr/dh-projects/discholed/-/issues/3) links to this problem).

- [Scrapper](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/Python%20Scripts/Scrapper/scrapper.py): The scrapper was made for me to retrieve every link of the applications in [e-editiones](https://www.e-editiones.org/map/).

- [Tag Finder](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/Python%20Scripts/Tag%20Finder/tag_finder.py): This script allows me to find specific tags in HTML and/or XML files. At the bottom of the script, I also coded a way to generate an odd for all the attribute places for this [issue](https://gitlab.inria.fr/dh-projects/discholed/-/issues/9).

- [Template Usage](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/Python%20Scripts/Template%20Usage/template_finder.py): The template finder helped us determine what type of template the XML files were using (the [issue](https://gitlab.inria.fr/dh-projects/discholed/-/issues/13)).

- [Timeline](https://gitlab.inria.fr/dh-projects/discholed/-/blob/main/Python%20Scripts/Timeline/timeline_generator.py): This script generates the needed file for the `<pb-timeline>`.