import os
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom
from tqdm import tqdm

# Liste des chemins d'accès aux corpus

corpus = [
    r'..\..\Discholed\data\pec\corpus',
    r'..\..\Discholed\data\bi\corpus',
    r'..\..\Discholed\data\cds\corpus',
    r'..\..\Discholed\data\ehri\corpus',
    r'..\..\Discholed\data\nachlassprojekt\corpus',
]


def timeline_generator(corpus_paths):
    for corpus_path in corpus_paths:
        absolute_path = os.path.abspath(corpus_path)
        folder_name = absolute_path.split('\\')[-2]

        # Create the root element <timeline>
        root = ET.Element('timeline')

        # Parcourir chaque fichier du corpus

        for filename in os.listdir(absolute_path) and tqdm(os.listdir(absolute_path), desc=folder_name, colour='#001eff'):
                if filename.endswith('.xml'):  # Vérifier que le fichier est un fichier XML
                    with open(os.path.join(absolute_path, filename), 'r', encoding='utf-8') as xml_file:
                        xml_content = xml_file.read()
                        # Charger le contenu XML dans Beautiful Soup
                        soup = BeautifulSoup(xml_content, "xml")

                        if folder_name == "pec":
                            # Trouver toutes les balises <p>
                            date = soup.find("profileDesc").find('correspDesc').find('correspAction').find('date')
                            title = soup.find("fileDesc").find('titleStmt').find('title', attrs={'xml:lang': 'en'})
                            # Créer l'élément <date> avec l'attribut "when"
                            date_element = ET.SubElement(root, 'date')
                            date_element.set('when', format_date(date['when-iso'], filename))

                            # Créer l'élément <ref> avec les attributs "target", "title" et "folder_name"
                            ref_element = ET.SubElement(date_element, 'ref')
                            ref_element.set('target', filename)
                            ref_element.set('title', title.text)
                            ref_element.set('folder_name', folder_name)

                        if folder_name == "bi":
                            # Trouver toutes les balises <p>
                            date_element = ET.SubElement(root, 'date')
                            try:
                                date = soup.find("sourceDesc").find('msDesc').find('msContents').find('msItem').find('docDate')
                                date_element.set('when', format_date(date['when'], filename))
                            except Exception:
                                date = "?"
                                date_element.set('when', date)
                            title = soup.find("fileDesc").find('titleStmt').find('title', attrs={'xml:lang': 'en'})

                            # Créer l'élément <ref> avec les attributs "target", "title" et "folder_name"
                            ref_element = ET.SubElement(date_element, 'ref')
                            ref_element.set('target', filename)
                            ref_element.set('title', title.text)
                            ref_element.set('folder_name', folder_name)

                        if folder_name == "cds":
                            # Trouver toutes les balises <p>
                            date = soup.find("profileDesc").find('correspDesc').find('correspAction').find('date')
                            title = soup.find("fileDesc").find('titleStmt').find('title', attrs={'xml:lang': 'en'})
                            # Créer l'élément <date> avec l'attribut "when"
                            date_element = ET.SubElement(root, 'date')
                            date_element.set('when', format_date(date['when-iso'], filename))

                            # Créer l'élément <ref> avec les attributs "target", "title" et "folder_name"
                            ref_element = ET.SubElement(date_element, 'ref')
                            ref_element.set('target', filename)
                            ref_element.set('title', title.text)
                            ref_element.set('folder_name', folder_name)

                        if folder_name == "nachlassprojekt":
                            # Trouver toutes les balises <p>
                            if soup.find("correspDesc"):
                                date_element = ET.SubElement(root, 'date')
                                try:
                                    date = soup.find("correspDesc").find('correspAction', attrs={'type': 'sent'}).find('date')
                                    date_element.set('when', format_date(date['when-iso'], filename))
                                except Exception:
                                    date = "?"
                                    date_element.set('when', date)
                            else:
                                date_element = ET.SubElement(root, 'date')
                                try:
                                    date = soup.find("history").find('origin').find('p').find('origDate')
                                    date_element.set('when', format_date(date['when-iso'], filename))
                                except Exception:
                                    date = "?"
                                    date_element.set('when', date)
                            title = soup.find("fileDesc").find('titleStmt').find('title', attrs={'xml:lang': 'en'})

                            # Créer l'élément <ref> avec les attributs "target", "title" et "folder_name"
                            ref_element = ET.SubElement(date_element, 'ref')
                            ref_element.set('target', filename)
                            ref_element.set('title', title.text)
                            ref_element.set('folder_name', folder_name)

                        if folder_name == "ehri":
                            # Trouver toutes les balises <p>
                            date_element = ET.SubElement(root, 'date')
                            try:
                                date = soup.find("profileDesc").find('creation').find('date')
                                date_element.set('when', format_date(date['when'], filename))
                            except Exception:
                                date = "?"
                                date_element.set('when', date)
                            title = soup.find("fileDesc").find('titleStmt').find('title', attrs={'xml:lang': 'en'})

                            # Créer l'élément <ref> avec les attributs "target", "title" et "folder_name"
                            ref_element = ET.SubElement(date_element, 'ref')
                            ref_element.set('target', filename)
                            ref_element.set('title', title.text)
                            ref_element.set('folder_name', folder_name)

        # Créer l'objet ElementTree et enregistrer le document XML dans un fichier
        tree = ET.ElementTree(root)

        # Créer une chaîne de caractères contenant le document XML indenté
        xml_string = minidom.parseString(ET.tostring(root)).toprettyxml(indent='    ')
        xml_string = '\n'.join(xml_string.split('\n')[1:])

        # Enregistrer le document XML dans un fichier avec l'en-tête XML
        with open(f'timeline_{folder_name}.xml', 'w', encoding='utf-8') as xml_file:
            xml_file.write(xml_string)

def format_date(date, filename):
    parts = date.split('-')
    year = parts[0]
    month = parts[1] if len(parts) > 1 else '01'
    day = parts[2] if len(parts) > 2 else '01'
    formatted_date = f"{year}-{month}-{day}"
    return formatted_date

timeline_generator(corpus)