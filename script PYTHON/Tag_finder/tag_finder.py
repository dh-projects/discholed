import os
from tqdm import tqdm
from bs4 import BeautifulSoup

tag = "place"

def finder_html(tag):
    app_path = [r'..\..\Discholed']
    for corpus_path in app_path:
        absolute_path = os.path.abspath(corpus_path)
        folder_name = absolute_path.split('\\')[-2]

        # Parcourir chaque fichier du corpus en incluant les fichiers des sous-répertoires
        for root, _, filenames in os.walk(absolute_path):
            for filename in filenames:
                if filename.endswith('.html'):  # Vérifier que le fichier est un fichier HTML
                    with open(os.path.join(root, filename), 'r', encoding='utf-8') as html_file:
                        html_content = html_file.read()
                        # Charger le contenu HTML dans Beautiful Soup
                        soup = BeautifulSoup(html_content, "html.parser")
                        # Trouver toutes les balises avec le tag spécifié
                        paragraphs = soup.find_all(tag)
                        if paragraphs:
                            print(filename,"\n",paragraphs)

def finder_xml(tag, app_path):
    for corpus_path in app_path:
        absolute_path = os.path.abspath(corpus_path)
        folder_name = absolute_path.split('\\')[-2]

        # Parcourir chaque fichier du corpus en incluant les fichiers des sous-répertoires
        all_attr = []
        other_attr = []
        for root, _, filenames in os.walk(absolute_path) and tqdm(os.walk(absolute_path), colour='magenta'):

            for filename in filenames:
                if filename.endswith('Ortsindex.xml'):  # Vérifier que le fichier est un fichier XML
                    with open(os.path.join(root, filename), 'r', encoding='utf-8') as xml_file:
                        xml_content = xml_file.read()
                        # Charger le contenu XML dans Beautiful Soup
                        soup = BeautifulSoup(xml_content, "xml")
                        # Trouver toutes les balises avec le tag spécifié
                        placenames = soup.find_all(tag)
                        if placenames:
                            for placename in placenames:
                                if 'type' in placename.attrs:
                                    placetype = placename['type']
                                    if placetype not in all_attr:
                                        all_attr.append(placetype)
                                else:
                                    if not placename.attrs:
                                        other_attr.append(filename)
        print(all_attr, other_attr)


#finder_xml(tag,[r'..\..\Discholed'])

list = ['country', 'region', 'city', 'plain', 'river', 'continent', 'sea', 'castle', 'village', 'island', 'peninsula', 'lake', 'valley', 'mountain', 'pass', 'castel', 'fort', 'monastery', 'state', 'mansion', 'building', 'waterfall', 'town', 'strait', 'district', 'municipality', 'hamlet', 'house', 'street', 'square', 'archeological_site', 'department', 'embassy', 'stream', 'camp', 'canton', 'park', 'canal', 'theater', 'cathedral', 'thoroughfare', 'empire', 'forest', 'gulf', 'hotel', 'townhall', 'ccuntry', 'steam', 'ocean', 'monument', 'quarter', 'point', 'port', 'church', 'university', 'boulevard', 'station', 'place', 'hill', 'opera', 'college', 'circus', 'venue', 'cabaret', 'duchy', 'museum', 'upland', 'settlement', 'location', 'mountains']
sorted_list =sorted(list)

for attribute in sorted_list:
    # Capitalize the attribute by splitting the words, capitalizing each word, and joining them back
    capitalized_attribute = ' '.join(word.capitalize() for word in attribute.split('_'))

    # Generate the XML code using the attribute and capitalized_attribute
    xml_code = '''
    <model predicate=".//place[@type=('{}')]" behaviour="block">
        <param name="content" value="for $n in .//place[@type=('{}')] let $type := $n/@type group by $ref := $n/placeName[1] order by $ref return $n"/>
    <pb:template xmlns="" xml:space="preserve">
    <pb-collapse>
        <div slot="collapse-trigger" class='card_title'>
            <h2>{}</h2>
        </div>
        <div slot="collapse-content" class='card_index'>
            <ul style="overflow: auto; max-height: 200px; min-height: auto;">
                [[content]]
            </ul>
        </div>
    </pb-collapse>
    </pb:template>
    <outputRendition xml:space="preserve">
        background-color: #DEE2E6;
        border-radius: 10px;
    </outputRendition>
    </model>
    '''.format(attribute, attribute, capitalized_attribute)

    # Print the generated XML code
    print(xml_code)
    print()


