import os
import re
from tqdm import tqdm
import pandas as pd

# Liste des chemins d'accès aux corpus
corpus = [
    r'..\..\Discholed\data\nachlassprojekt\corpus',
]

# Fonction index qui vérifie si les <pb> ont bien des balises <p> pour éviter le bug d'affichage des index
def template_finder(corpus_paths):
    final_lists = []
    for corpus_path in corpus_paths:
        absolute_path = os.path.abspath(corpus_path)
        folder_name = absolute_path.split('\\')[-2]

        # Parcourir chaque fichier du corpus
        for filename in os.listdir(absolute_path) and tqdm(os.listdir(absolute_path), desc=folder_name, colour='magenta'):
            pb_list = []
            if filename.endswith('.xml'):  # Vérifier que le fichier est un fichier XML
                with open(os.path.join(absolute_path, filename), 'r', encoding='utf-8') as xml_file:
                    xml_content = xml_file.read()
                    # Utiliser une expression régulière pour trouver les tags recherchés
                    pattern = r'<\?teipublisher template="([^"]+)"\?>'
                    matches = re.findall(pattern, xml_content)
                    if matches:
                        for match in matches:
                            final_lists += [match]
    print(final_lists)


template_finder(corpus)
