import requests

def close_all_issues(repo_owner, repo_name, access_token):
    headers = {
        'Authorization': f'token {access_token}',
        'Accept': 'application/vnd.github.v3+json'
    }
    url = f'https://api.github.com/repos/{repo_owner}/{repo_name}/issues?state=open'

    while True:
        response = requests.get(url, headers=headers)
        issues = response.json()

        if not issues:
            break

        for issue in issues:
            issue_number = issue['number']
            print(issue_number)
            close_url = f'https://api.github.com/repos/{repo_owner}/{repo_name}/issues/{issue_number}'
            payload = {
                'state': 'closed'
            }
            requests.patch(close_url, headers=headers, json=payload)

        if 'Link' not in response.headers:
            break

        link_header = response.headers['Link']
        next_page_url = None

        for link in link_header.split(','):
            if 'rel="next"' in link:
                next_page_url = link.strip().split(';')[0].strip('<>')

        if not next_page_url:
            break

        url = next_page_url

# Usage example
repo_owner = 'Samuel-Scalbert'
repo_name = 'GitHub-action-testing'
access_token = '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'

close_all_issues(repo_owner, repo_name, access_token)
