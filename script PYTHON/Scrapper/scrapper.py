import requests
from bs4 import BeautifulSoup

# Specify the URL you want to scrape
url = ""

# Send a GET request to the URL
response = requests.get(url)

# Check if the request was successful (status code 200)
if response.status_code == 200:
    # Parse the HTML content using BeautifulSoup
    soup = BeautifulSoup(response.content, "html.parser")

    # Find the <div> tag with id='navlist'
    div_navlist = soup.find("div", id="navlist")
    if div_navlist:
        # Find all <paper-item> tags within the <div>
        paper_items = div_navlist.select("paper-item")

        # Print the text content of each <paper-item>
        for paper_item in paper_items:
            text_content = paper_item.get_text(strip=True)
            print(text_content)
    else:
        print("No <div> tag with id='navlist' found.")
else:
    print("Failed to retrieve the webpage. Status code:", response.status_code)

