import os
from tqdm import tqdm
import pandas as pd
from bs4 import BeautifulSoup

# Liste des chemins d'accès aux corpus
corpus = [
    r'..\..\Discholed\data\pec\corpus',
    r'..\..\Discholed\data\bi\corpus',
    r'..\..\Discholed\data\cds\corpus',
    r'..\..\Discholed\data\rochlitz\corpus',
    r'..\..\Discholed\data\maguerre\corpus',
    r'..\..\Discholed\data\nachlassprojekt\corpus'
]


# Fonction index qui vérifie si les <pb> ont bien des balises <p> pour éviter le bug d'affichage des index
def index(corpus_paths):
    final_lists = []
    for corpus_path in corpus_paths:
        print(corpus_path)
        absolute_path = os.path.abspath(corpus_path)
        folder_name = absolute_path.split('\\')[-2]

        # Parcourir chaque fichier du corpus
        for filename in os.listdir(absolute_path) and tqdm(os.listdir(absolute_path), desc=folder_name, colour='magenta'):
            pb_list = []
            if filename.endswith('.xml'):  # Vérifier que le fichier est un fichier XML
                with open(os.path.join(absolute_path, filename), 'r', encoding='utf-8') as xml_file:
                    xml_content = xml_file.read()
                    # Charger le contenu XML dans Beautiful Soup
                    soup = BeautifulSoup(xml_content, "xml")

                    # Trouver toutes les balises <p>
                    paragraphs = soup.find_all("p")

                    # Parcourir les balises <p> et leurs enfants
                    for paragraph in paragraphs:
                        pb = paragraph.find_all("pb")
                        if len(pb) >= 2:  # Vérifier que chaque <p> a au moins 2 ou plus balises <pb>
                            for pb_nb in pb:
                                if pb_nb != pb[-1]:  # Ne pas traiter le dernier <pb>
                                    if pb_nb.find_next() != pb_nb.find_next(
                                            "pb"):  # Vérifier si la balise <pb> est vide ou si c'est une annexe
                                        try:
                                            pb_list.append(pb_nb['n'])  # Ajouter le numéro du <pb> dans une liste
                                        except KeyError:
                                            pb_list.append(
                                                pb_nb[
                                                    'xml:id'])  # Ajouter l'identifiant de page dans la liste pour les éditions de "maguerre"
            # Si dans le fichier il y a un <pb> sans <p> alors, on l'ajoute à notre liste de fin
            if len(pb_list) > 0:
                final_lists.append([filename, pb_list, folder_name])
    # Créer un DataFrame pandas à partir de la liste de listes contenant les <pb> sans <p>
    df = pd.DataFrame(final_lists, columns=['nom de fichier', 'numéro de page', 'nom de dossier'])
    # Écrire les données dans un fichier CSV
    df.to_csv('file_to_modify.csv')


index(corpus)
# regex pour changer rapidement dans les fichiers : (<pb\s+[^>]*n=["'](?:12|74|75|76|77|78|79)["'][^>]*/>)
